﻿using Unity.Entities;
using Unity.Mathematics;

namespace Cynics
{
    [GenerateAuthoringComponent]
    public struct MouseData : IComponentData
    {
        public float3 screenPosition;
        public float3 screenDelta;
        public float3 worldPosition;
        public float3 worldDelta;
        public float2 mouseScrollDelta;
    }
}
