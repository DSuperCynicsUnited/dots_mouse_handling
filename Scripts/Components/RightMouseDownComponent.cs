﻿using System;
using Unity.Entities;
using Unity.Mathematics;

namespace Cynics
{
    [Serializable]
    public struct RightMouseDownComponent : IComponentData
    {
        public float3 screenPosition;
        public float3 worldPosition;
        public double timeStamp;
    }
}
