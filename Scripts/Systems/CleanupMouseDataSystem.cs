﻿using Unity.Entities;

namespace Cynics
{
    

    public class CleanupMouseDataSystem : SystemBase
    {
        private EndSimulationEntityCommandBufferSystem _endSimulationEntityCommandBufferSystem;
        private EntityManager _entityManager;


        protected override void OnCreate()
        {
            base.OnCreate();
            _endSimulationEntityCommandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
            _entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        }

        protected override void OnUpdate()
        {

            var ecb = _endSimulationEntityCommandBufferSystem.CreateCommandBuffer().ToConcurrent();

            Entities.WithAll<Tag_MouseOutsideScreen>()
                .ForEach((Entity e, int entityInQueryIndex, in MouseData position) =>
                {
                    ecb.RemoveComponent<Tag_MouseOutsideScreen>(entityInQueryIndex,e);
                }).ScheduleParallel();

            Entities.WithAll<Tag_LeftMouseDownThisFrame>()
                .ForEach((Entity e, int entityInQueryIndex, in MouseData position) =>
                {
                    ecb.RemoveComponent<Tag_LeftMouseDownThisFrame>(entityInQueryIndex,e);
                }).ScheduleParallel();

            Entities.WithAll<Tag_LeftMouseUpThisFrame>()
                .ForEach((Entity e, int entityInQueryIndex, in MouseData position) =>
                {
                    ecb.RemoveComponent<Tag_LeftMouseUpThisFrame>(entityInQueryIndex,e);
                }).ScheduleParallel();

            Entities.WithAll<Tag_RightMouseDownThisFrame>()
                .ForEach((Entity e, int entityInQueryIndex, in MouseData position) =>
                {
                    ecb.RemoveComponent<Tag_RightMouseDownThisFrame>(entityInQueryIndex,e);
                }).ScheduleParallel();

            Entities.WithAll<Tag_RightMouseUpThisFrame>()
                .ForEach((Entity e, int entityInQueryIndex, in MouseData position) =>
                {
                    ecb.RemoveComponent<Tag_RightMouseUpThisFrame>(entityInQueryIndex,e);
                }).ScheduleParallel();
            
            Entities.WithAll<Tag_MiddleMouseDownThisFrame>()
                .ForEach((Entity e, int entityInQueryIndex, in MouseData position) =>
                {
                    ecb.RemoveComponent<Tag_MiddleMouseDownThisFrame>(entityInQueryIndex,e);
                }).ScheduleParallel();

            Entities.WithAll<Tag_MiddleMouseUpThisFrame>()
                .ForEach((Entity e, int entityInQueryIndex, in MouseData position) =>
                {
                    ecb.RemoveComponent<Tag_MiddleMouseUpThisFrame>(entityInQueryIndex,e);
                }).ScheduleParallel();

            Entities.WithAll<LeftMouseDownComponent, LeftMouseUpComponent>().ForEach(
                (Entity e, int entityInQueryIndex, in MouseData position) =>
                {
                    ecb.RemoveComponent<LeftMouseDownComponent>(entityInQueryIndex,e);
                    ecb.RemoveComponent<LeftMouseUpComponent>(entityInQueryIndex,e);

                }).ScheduleParallel();

            Entities.WithAll<RightMouseDownComponent, RightMouseUpComponent>().ForEach(
                (Entity e, int entityInQueryIndex, in MouseData position) =>
                {
                    ecb.RemoveComponent<RightMouseDownComponent>(entityInQueryIndex,e);
                    ecb.RemoveComponent<RightMouseUpComponent>(entityInQueryIndex,e);

                }).ScheduleParallel();

            Entities.WithAll<MiddleMouseDownComponent, MiddleMouseUpComponent>().ForEach((Entity e, int entityInQueryIndex, in MouseData position) =>
            {
                ecb.RemoveComponent<MiddleMouseDownComponent>(entityInQueryIndex,e);
                ecb.RemoveComponent<MiddleMouseUpComponent>(entityInQueryIndex,e);

            }).ScheduleParallel();
            _endSimulationEntityCommandBufferSystem.AddJobHandleForProducer(this.Dependency);
        }
    }

}
