﻿using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Cynics
{
    [UpdateInGroup(typeof(SimulationSystemGroup), OrderFirst = true)]
    public class MouseDataSystem : SystemBase
    {
        private Camera _mainCamera;

        private bool _firstFrame = true;
        
        protected override void OnUpdate()
        {
            if (_mainCamera == null)
            {
                _mainCamera = Camera.main;
            }
            
            var elapsedTime = Time.ElapsedTime;
            var currentScreenPosition = (float3)Input.mousePosition;
            var currentWorldPosition = (float3) _mainCamera.ScreenToWorldPoint(currentScreenPosition);
            var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            var mouseScrollDelta = (float2) Input.mouseScrollDelta;
            
            Entities.WithStructuralChanges().ForEach(
                (Entity e, int entityInQueryIndex, ref MouseData mouseData) =>
                {
                    mouseData.screenDelta =  (_firstFrame) ? new float3() :  mouseData.screenPosition - currentScreenPosition;
                    mouseData.worldDelta = (_firstFrame) ? new float3() : mouseData.worldPosition - currentWorldPosition;
                    mouseData.screenPosition = currentScreenPosition;
                    mouseData.worldPosition = currentWorldPosition;
                    mouseData.mouseScrollDelta = mouseScrollDelta;
                    
                    
                    if (currentScreenPosition.x < 0
                        || currentScreenPosition.y < 0
                        || currentScreenPosition.x > Screen.width
                        || currentScreenPosition.y > Screen.height)
                    {
                        entityManager.AddComponent<Tag_MouseOutsideScreen>(e);
                    }

                    if (Input.GetMouseButtonDown(0))
                    {
                        entityManager.AddComponentData(e,
                            new LeftMouseDownComponent
                            {
                                screenPosition = currentScreenPosition,
                                worldPosition = currentWorldPosition,
                                timeStamp = elapsedTime
                            });
                        entityManager.AddComponent<Tag_LeftMouseDownThisFrame>(e);
                    }
                    
                    if (Input.GetMouseButtonUp(0))
                    {
                        entityManager.AddComponentData(e,
                            new LeftMouseUpComponent
                            {
                                screenPosition =  currentScreenPosition,
                                worldPosition = currentWorldPosition,
                                timeStamp = elapsedTime
                            });
                        entityManager.AddComponent<Tag_LeftMouseUpThisFrame>(e);
                    }

                    if (Input.GetMouseButtonDown(1))
                    {
                        entityManager.AddComponentData(e,
                            new RightMouseDownComponent
                            {
                                screenPosition = currentScreenPosition,
                                worldPosition = currentWorldPosition,
                                timeStamp = elapsedTime
                            });
                        entityManager.AddComponent<Tag_RightMouseDownThisFrame>(e);
                    }

                    if (Input.GetMouseButtonUp(1))
                    {
                        entityManager.AddComponentData(e,
                            new RightMouseUpComponent
                            {
                                screenPosition =  currentScreenPosition,
                                worldPosition = currentWorldPosition,
                                timeStamp = elapsedTime
                            });
                        entityManager.AddComponent<Tag_RightMouseUpThisFrame>(e);
                    }
                    
                    if (Input.GetMouseButtonDown(2))
                    {
                        entityManager.AddComponentData(e,
                            new MiddleMouseDownComponent
                            {
                                screenPosition = currentScreenPosition,
                                worldPosition = currentWorldPosition,
                                timeStamp = elapsedTime
                            });
                        entityManager.AddComponent<Tag_MiddleMouseDownThisFrame>(e);
                    }
                    
                    if (Input.GetMouseButtonUp(2))
                    {
                        entityManager.AddComponentData(e,
                            new MiddleMouseUpComponent
                            {
                                screenPosition =  currentScreenPosition,
                                worldPosition = currentWorldPosition,
                                timeStamp = elapsedTime
                            });
                        entityManager.AddComponent<Tag_MiddleMouseUpThisFrame>(e);
                    }
                }).Run();

            _firstFrame = false;
        }
    }
}
