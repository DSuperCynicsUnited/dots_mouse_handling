# DOTS_Mouse_Handling #

This is a simple system and components I put together for tracking mouse data in a DOTS oriented way.

Project version 0.1.0

### Unity Packages
Built to work with Unity 2019.4.3 or 2020.1.0beta15. 

* Entities 0.11.1-preview4
* Mathematics 1.1.0

Compatability may change with newer versions

### How do I get set up? ###

* Copy the folder into your project.
* Create a new GameObject in your scene that will be converted to an entity either by placing it in a subscene or adding the ConvertToEntity script.
* Attach a Mouse Data component to the game object.
* Attach a Script Optimize Entity script to the game object.
* The MouseData entity and component must be a singleton.

### What does it do? ###

The MouseData singleton entity keeps track of all the user's mouse behaviour. 

The MouseDataSystem runs at the beginning of the SimulationSystemGroup and updates the MouseData component as well as adding additional components and tags to capture important mouse behaviors.

Example : When the player clicks the left mouse button down, a LeftMouseDown component and a Tag_LeftMouseDownThisFrame is added, which tracks the screen and world positions where the Down event happened, and a timestamp.

The CleanupMouseDataSystem runs as the end of the SimulationSystemGroup and cleans up any tags or pairs of MouseDown and MouseUp components.

### What Are The Components##


* Mouse Data
~~~

	public struct MouseData : IComponentData
	{
   		public float3 screenPosition;
    	public float3 screenDelta;
    	public float3 worldPosition;
    	public float3 worldDelta;
		public float2 mouseScrollDelta;
	}

~~~



* LeftMouseDown
* LeftMouseUp
* RightMouseDown
* RightMouseUp
* MiddleMouseDown
* MiddleMouseUp

>All mouse down and up components have the same data.
~~~


	public struct LeftMouseDownComponent : IComponentData
	{
		public float3 screenPosition;
    	public float3 worldPosition;
    	public double timeStamp;
	}

~~~

* Tag_MouseOutsideScreen
* Tag_LeftMouseDownThisFrame
* Tag_LeftMouseUpThisFrame
* Tag_RightMouseDownThisFrame
* Tag_RightMouseUpThisFrame
* Tag_MiddleMouseDownThisFrame


### How Do I Use It? ###

Here's an example for a simple click anywhere inside the screen.

~~~

using Unity.Entities;

public class SimpleClick : SystemBase
{
	protected override void OnUpdate()
	{
		Entities.WithAll<LeftMouseDownComponent, LeftMouseUpComponent>().WithNone<Tag_MouseOutsideScreen>()
    		.ForEach((in MouseData mouseData) =>
    		{
				///Handle the click here.
				
    		}).Schedule();
	}
}

~~~

Here's a more complicated approach for something like a drag.

~~~

using Unity.Entities;

public class MouseDrag : SystemBase
{
	private bool _dragStarted;
	
	protected override void OnUpdate()
	{
		Entities.WithAll<LeftMouseDownComponent, Tag_LeftMouseDownThisFrame>().WithNone<Tag_MouseOutsideScreen>()
    		.ForEach((in MouseData mouseData) =>
    		{
				_dragStarted = true;
				
				// Handle drag start.
							
    		}).Schedule();
		
		Entities.WithAll<LeftMouseDownComponent>().WithNone<Tag_MouseOutsideScreen, Tag_LeftMouseDownThisFrame>()
			.ForEach((in MouseData mouseData) =>
			{
				if(! _dragStarted)
					continue;
					
				// Handle drag update
				
			}).Schedule();

		Entities.WithAll<LeftMouseDownComponent, RightMouseDownComponent, RightMouseDownThisFrame>()
			.WithNone(Tag_MouseOutsideScreen)
			.ForEach((in MouseData mouseData) =>
			{
				if(! _dragStarted)
					continue;
				
				//Handle drag end.

				_dragStarted = false;
			}).Schedule();
	}
}

~~~

### Extending the Mouse Handling System ###

Extending the mouse handling system is pretty easy, even in hybrid systems.

One simple trick you can add is to create more tags to add to the MouseData entity.

In the project is one example, Tag_MouseOverPlayArea.

The project I developed this for uses the new UI Toolbox and UI Builder to build the screen space game UI. This UI has an empty VisualElement PlayArea which covers the area not covered by other UI controls.

The PlayArea subscribes to the MouseOver and MouseOut events, adding and removing Tag_MouseOverPlayArea. 

This allows me to keep track of whether the mouse is over the UI or over the play area, and respond accordingly in my systems.

Conveniently, since the UI Toolbox code is all MonoBehaviours, these tags are added and removed on the main thread before the SimulationSystemGroup runs. 

My systems can use them without worrying about additional sync points.

